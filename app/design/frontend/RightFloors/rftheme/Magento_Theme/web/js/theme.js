/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

define([
    'jquery',
    'mage/smart-keyboard-handler',
    'mage/mage',
    'mage/ie-class-fixer',
    'Magento_Ui/js/modal/modal',
     'Magento_Ui/js/timer/countdownTimer',
    'domReady!'
], 



    function ($, keyboardHandler,modal,countdowntimer) {
    'use strict';

    

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

    $(document).ready(function(){
      
      $('li.special-menu ul li:last-child a').attr('href','/vinyl-planks/thickness.html?cat=1411');
      
        var d = new Date();
          d.setTime(d.getTime() + (120*60*1000));
          var expires = "expires="+ d.toUTCString();
        
          var today = new Date();
           var date = today.getFullYear()+'/'+(today.getMonth()+1)+'/'+today.getDate();
            var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
            var dateTime = date+' '+time;
        console.log("cookie="+getCookie('popupshow'));
		if(getCookie('popupshow') == 1 || getCookie('popupshow') == ""){
		    var options = {
                type: 'popup',
                responsive: true,
                innerScroll: false,
                modalClass: 'christmas-offer-container'
               
            };
            
          
            
         var modal =  $('#timermodal').modal(options).modal('openModal');
          modal.on('modalclosed', function () {
               $('.modals-wrapper').hide();
            });
          setTimeout(function(){
             $("#popuptimerd").countdowntimer({
              startDate : dateTime,
              dateAndTime : "2020/12/15 17:00:00",
             displayFormat : "D"
            });
            $("#popuptimerm").countdowntimer({
              startDate : dateTime,
              dateAndTime : "2020/12/15 17:00:00",
              displayFormat : "D:H:M"
            });
            $("#popuptimerh").countdowntimer({
              startDate : dateTime,
              dateAndTime : "2020/12/15 17:00:00",
              displayFormat : "D:H"
            });
            	$("#popuptimers").countdowntimer({
              startDate : dateTime,
              dateAndTime : "2020/12/15 17:00:00",
              displayFormat : "D:H:M:S"
            });
              
          },8000);
           
            document.cookie = "popupshow=0; expires="+expires+"; path=/";
		}
          
		
         
          console.log(dateTime+".....2020/12/15 17:00:00");
          setTimeout(function(){
            
              $("#countdowndays").countdowntimer({
                startDate : dateTime,
                dateAndTime : "2020/12/15 17:00:00",
                displayFormat : "D"
              });
              $("#countdownhours").countdowntimer({
                      startDate : dateTime,
                      dateAndTime : "2020/12/15 17:00:00",
                      displayFormat : "D:H"
                    });
              $("#countdownmin").countdowntimer({
                      startDate : dateTime,
                      dateAndTime : "2020/12/15 17:00:00",
                      displayFormat : "D:H:M"
                    });
              $("#countdownsec").countdowntimer({
                      startDate : dateTime,
                      dateAndTime : "2020/12/15 17:00:00",
                     displayFormat : "D:H:M:S"
          
                    });
          },8000);
       
    });
   

    if ($('body').hasClass('checkout-cart-index')) {
        if ($('#co-shipping-method-form .fieldset.rates').length > 0 &&
            $('#co-shipping-method-form .fieldset.rates :checked').length === 0
        ) {
            $('#block-shipping').on('collapsiblecreate', function () {
                $('#block-shipping').collapsible('forceActivate');
            });
        }
    }

    $('.cart-summary').mage('sticky', {
        container: '#maincontent'
    });

    $('.panel.header > .header.links').clone().appendTo('#store\\.links');

    keyboardHandler.apply();
});

define([
    'jquery',
    'mage/smart-keyboard-handler',
    'mage/mage',
    'mage/ie-class-fixer',
    'domReady!'
], 

    function ($) { $('.label').click( function() {
    $("#searchtog").toggleClass("active");
} ); }
   
    );
